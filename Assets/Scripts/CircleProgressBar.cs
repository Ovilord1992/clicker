﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircleProgressBar : MonoBehaviour
{
    public Image BombCircleImage;
    public Image LazerCircleImage;
    public Image ShieldCircleImage;
    public Image HillCircleImage;

    public BusterController bc;

    public void updateAll()
    {
        updateBombCircle();
        updateHillCircle();
        updateShieldCircle();
        updateLazerCircle();
    }

    public void updateBombCircle()
    {
        float value = ((float)bc.BombEnergy / (float)bc.EnergyToBomb);
        if (value<= bc.EnergyToBomb)
            BombCircleImage.fillAmount = 1;
        BombCircleImage.fillAmount = value;
    }

    public void updateHillCircle()
    {
        float value = ((float)bc.HillEnergy / (float)bc.EnergyToHill);
        if (value <= bc.EnergyToHill)
            HillCircleImage.fillAmount = 1;
        HillCircleImage.fillAmount = value;
    }

    public void updateShieldCircle()
    {
        float value = ((float)bc.ShieldEnergy / (float)bc.EnergyToShield);
        if (value <= bc.EnergyToShield)
            ShieldCircleImage.fillAmount = 1;
        ShieldCircleImage.fillAmount = value;
    }

    public void updateLazerCircle()
    {
        float value = ((float)bc.LazerEnergy / (float)bc.EnergyToLazer);
        if (value <= bc.EnergyToLazer)
            LazerCircleImage.fillAmount = 1;
        LazerCircleImage.fillAmount = value;
    }




}
