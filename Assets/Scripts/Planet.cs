﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Planet : MonoBehaviour
{
    public BustersHideShow bustersHideAndShow;

    [SerializeField]
    GameObject HilledEffect;

    [SerializeField]
    GameObject EndGamePanel;

    [SerializeField]
    GameObject PauseButton;

    [SerializeField]
    TMP_Text PanelBestScore;

    [SerializeField]
    TMP_Text PanelComins;

    [SerializeField]
    GameStats gs;


    public Animator MainCameraAC;
    public Animator PlanetSkinAC;
    public GameObject GameManager;
    public GameObject bc;
    public GameObject HillButton;
    public GameObject ShieldButton;
    public NumbersPull pull;
    public BustersHideShow bhs;
    public int PlanetHealth;

    [SerializeField]
    ParticleSystem deathObj;

    Vector3 PlanetPosition;

    // Количество восстанавливаемых жизней
    public int hill_value;
    // Время неуязвимости

    // состояние неуязвимости
    bool isInvalnerable;

    float TimeWashHilled = 3f;

    private void Awake()
    {
        PlanetPosition = transform.position;
        bc = GameObject.FindWithTag("BusterController");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Comet"))
        {
            if (!isInvalnerable)
            {
                int health = collision.gameObject.GetComponent<Comet>().CometHealth;
                PlanetHealth -= health;
                MainCameraAC.SetTrigger("Shake");
                PlanetSkinAC.SetTrigger("WasHitted");
                pull.SpawnNumber(collision.transform.position, health,Color.red);
            }
            else
            {
                MainCameraAC.SetTrigger("Shake");
            }

            Instantiate(deathObj, collision.transform.position, Quaternion.identity);

            Destroy(collision.gameObject);

            CheckHealth();
        }



    }

    public void CheckHealth()
    {
        if (PlanetHealth <= 0)
        {


            UpgradePanelCounters();
            EndGamePanel.SetActive(true);
            PauseButton.SetActive(false);
        }
    }

    private void UpgradePanelCounters()
    {
        PanelComins.text = gs.MainComins.ToString();
        PanelBestScore.text = gs.MainScore.ToString();
    }

    public void OnHill()
    {
            int energyToHill = bc.GetComponent<BusterController>().EnergyToHill;
            if (bc.GetComponent<BusterController>().HillEnergy >= energyToHill)
            {

                StartCoroutine("wasHilled");
                PlanetHealth += hill_value;
                bc.GetComponent<BusterController>().HillEnergy -= energyToHill;
                bhs.HideAllBusters();
                bhs.HillInvokeShowAllBusters();

            }
            else
            {
                HillButton.GetComponent<BustersAnims>().NotEnoughEnergy();
            }
    }

    IEnumerator wasHilled()
    {
        GameObject go = Instantiate(HilledEffect, PlanetPosition, Quaternion.identity);
        PlanetSkinAC.SetTrigger("WasHilled");
        yield return new WaitForSeconds(TimeWashHilled);
        Destroy(go);
        yield return null;
    }

    public void OnProtected()
    {

        if (isInvalnerable)
            return;

        int energyToShield = bc.GetComponent<BusterController>().EnergyToShield;

        if (bc.GetComponent<BusterController>().ShieldEnergy >= energyToShield)
        {
            bustersHideAndShow.HideAllBusters();
            isInvalnerable = true;
            bc.GetComponent<BusterController>().ShieldEnergy -= energyToShield;
            Invoke("OffProtected", bc.GetComponent<BusterController>().ShieldTime);
            PlanetSkinAC.SetBool("isProtected", true);



        }

                 

    }

    private void OffProtected()
    {
        isInvalnerable = false;
        bustersHideAndShow.ShowAllBusters();
        PlanetSkinAC.SetBool("isProtected", false);
    }



    private void Update()
    {
        GetComponentInChildren<TMP_Text>().text = PlanetHealth.ToString();
    }
}
