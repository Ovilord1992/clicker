﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    public Camera Cam;
    public GameStats GameStats;
    public BusterController bc;
    public NumbersPull np;

    [SerializeField]
    GameObject GoldNumber;

    void Update()
    {
        if (!bc.prepareBombBuster
            && !bc.usingLazerBuster)
        {
            var touches = Input.touches;
            foreach (var t in touches)
            {
                if (t.phase == TouchPhase.Began)
                {

                    RaycastHit2D aHit = new RaycastHit2D();
                    aHit = Physics2D.Raycast(Cam.ScreenToWorldPoint(t.position), Vector2.zero);
                    if (aHit.transform != null)
                    {
                        if (aHit.transform.CompareTag("Comet"))
                        {
                            aHit.transform.gameObject.GetComponentInChildren<Animator>().SetTrigger("tap");
                            aHit.transform.GetComponent<Comet>().GetHitted(1);
                            GameStats.MainScore += 1;
                            bc.EnergyGrowth(aHit.transform.name, 1);

                            np.SpawnNumber(aHit.transform.position, 1, Color.red);
                        }
                        else if (aHit.transform.CompareTag("Comin"))
                        {
                            if (!aHit.transform.GetComponent<CominDeath>().clicked)
                            {
                                aHit.transform.GetComponent<CominDeath>().onTouch();
                                np.SpawnNumber(aHit.transform.position, aHit.transform.GetComponent<CominDeath>().comins, Color.yellow);
                            }

                        }
                    }
                }

            }
        }

    }
}
