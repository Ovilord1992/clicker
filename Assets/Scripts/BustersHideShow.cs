﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BustersHideShow : MonoBehaviour
{
    [SerializeField]
    GameObject[] Busters;

    public GameObject bc;
    public CircleProgressBar cpb;

    public float TimeToInvokeLazer;
    public float TimeToInvokeShield;

    private void Awake()
    {
        
    }

    public void LazerInvokeShowAllBusters()
    {
        Invoke("ShowAllBusters", bc.GetComponent<BusterController>().LazerTime);
    }

    public void ShieldInvokeShowAllBusters()
    {
        Invoke("ShowAllBusters", bc.GetComponent<BusterController>().ShieldTime);
    }

    public void BombInvokeShowAllBusters()
    {
        Invoke("ShowAllBusters", bc.GetComponent<BusterController>().BombTimeBtwExplosions);
    }

    public void HillInvokeShowAllBusters()
    {
        Invoke("ShowAllBusters", bc.GetComponent<BusterController>().BombTimeBtwExplosions);
    }

    public void ShowAllBusters()
    {
        foreach (GameObject go in Busters)
        {
            go.GetComponent<Button>().interactable = true;

            Color imag_c = go.GetComponentInChildren<Image>().color;
            imag_c.a = 1f;

            go.GetComponentInChildren<Image>().color = imag_c;

        }
    }

    public void HideAllBusters()
    {
        foreach (GameObject go in Busters)
        {
            go.GetComponent<Button>().interactable = false;

            Color imag_c = go.GetComponentInChildren<Image>().color;

            imag_c.a = 0.4f;

            go.GetComponentInChildren<Image>().color = imag_c;
        }


    }


}
