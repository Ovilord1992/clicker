﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpgradeAll : MonoBehaviour
{
    [SerializeField]
    private TMP_Text BestScoreText;
    [SerializeField]
    private TMP_Text CominsText;

    private void Start()
    {
        if(PlayerPrefs.HasKey("BestScore"))
            BestScoreText.text = PlayerPrefs.GetInt("BestScore").ToString();
        else
        {
            PlayerPrefs.SetInt("BestScore", 0);
            BestScoreText.text = "0";
        }
            
        if (PlayerPrefs.HasKey("Comins"))
            CominsText.text = PlayerPrefs.GetInt("Comins").ToString();
        else
        {
            PlayerPrefs.SetInt("Comins", 0);
            CominsText.text = "0";
        }

    }
}
