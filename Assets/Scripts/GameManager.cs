﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    GameStats gs;
    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OpenGameScene()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void OpenMenuScene()
    {
        SaveCurrent();
        Time.timeScale = 1f;
        SceneManager.LoadScene("MenuScene");
    }

    public void SaveCurrent()
    {

        if (gs.MainScore > PlayerPrefs.GetInt("BestScore"))
        {
            PlayerPrefs.SetInt("BestScore", gs.MainScore);
        }

        PlayerPrefs.SetInt("Comins", gs.MainComins + PlayerPrefs.GetInt("Comins"));
    }


}
