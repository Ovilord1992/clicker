﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CominDeath : MonoBehaviour
{

    Animator ac;
    public int comins;
    public bool clicked=false;

    [SerializeField]
    int lifeTIme = 10;



    [SerializeField]
    GameStats gs;

    private void Awake()
    {

        gs = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameStats>();
        ac = GetComponent<Animator>();
        comins = 1;
        StartCoroutine("AutoDestroyed");
    }

    IEnumerator AutoDestroyed()
    {
        yield return new WaitForSeconds(lifeTIme);
        if (!clicked)
        {
            
            clicked = true;
            ac.SetTrigger("destroyed");

        }

        yield return null;
    }

    public void onTouch()
    {

            if (!clicked)
            {
                gs.MainComins += comins;
                ac.SetTrigger("taked");
                clicked = true;
            }
        


    }

    public void OnDeath()
    {
        Destroy(transform.parent.gameObject);
    }
}
