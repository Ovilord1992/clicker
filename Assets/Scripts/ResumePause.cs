﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumePause : MonoBehaviour
{
    [HideInInspector]
    public static bool GameIsPaused = false;

    public GameObject PauseMenuUI;

    private void Awake()
    {
        ResumeGame();
    }
    public void PauseGame()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void ResumeGame()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
}
