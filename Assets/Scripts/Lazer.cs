using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lazer : MonoBehaviour
{
    public bool LazerIsReady;

    public BustersHideShow bustersHideAndShow;
    public CircleProgressBar cpb;
    private float LazerDistance = 16f;

    public Camera cam;
    public LayerMask lm;
    public Transform centerTransform;
    public BusterController bc;

    GameObject LazerBuster;
    [SerializeField]
    GameObject BlinkEffect;

    [SerializeField]
    NumbersPull np;

    public GameStats GameStats;

    public LineRenderer m_lineRenderer;

    //�������� ��������� �����
    public int timeBtwShoots = 6;
    private int changabletimeBtwShoots;

    void Awake()
    {
        changabletimeBtwShoots = timeBtwShoots;
    }

    void ShootLaser(Vector2 startPos, Vector2 aim)
    {
        RaycastHit2D hit = Physics2D.Raycast(startPos, aim, LazerDistance, lm);
        if (hit)
        {
            Draw2DRay(startPos, hit.point);
            GameObject go = hit.transform.gameObject;
            if (go.CompareTag("Comet"))
            {
                if (changabletimeBtwShoots <=0)
                {
                    Vector2 pos = go.transform.position;
                    go.GetComponent<Comet>().CometHealth--;
                    bc.EnergyGrowth(go.transform.name,1);
                    GameStats.MainScore++;
                    np.SpawnNumber(pos, 1,Color.red);
                    changabletimeBtwShoots = timeBtwShoots;
                    
                }
                else
                {
                    changabletimeBtwShoots -= 1;
                }
                
            }
        }
        else
        {
            Draw2DRay(startPos, 12*aim.normalized);
        }
    }

    private void Draw2DRay(Vector2 startPos, Vector2 endPos)
    {
        m_lineRenderer.SetPosition(0, startPos);
        m_lineRenderer.SetPosition(1, endPos);
        BlinkEffect.transform.position = endPos;
    }

    // Update is called once per frame
    void Update()
    {
        if(m_lineRenderer.gameObject.activeSelf){
            if (Input.touchCount > 0)
            {
                m_lineRenderer.enabled = true;
                Touch touch = Input.GetTouch(0);
                Vector2 localTouch = cam.ScreenToWorldPoint(touch.position);
                ShootLaser(centerTransform.position, localTouch);
            }
            else
            {
                m_lineRenderer.enabled = false;
            }
        }

    }

    public void StartRay()
    {
        
        if (!gameObject.activeSelf)
        {
            if (bc.isEnoughLazerEnergy())
            {
                m_lineRenderer.gameObject.SetActive(true);
                Invoke("StopRay", bc.LazerTime);
                bc.UsedLazerBuster();
                bustersHideAndShow.HideAllBusters();
                cpb.updateLazerCircle();
            }
        }
    }

    void StopRay()
    {
        m_lineRenderer.gameObject.SetActive(false);
        bc.GetComponent<BusterController>().usingLazerBuster = false;
        bustersHideAndShow.ShowAllBusters();
        BlinkEffect.transform.position = new Vector3(20, 20, 0);
    }





}
