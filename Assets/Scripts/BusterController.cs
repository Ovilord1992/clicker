﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BusterController : MonoBehaviour
{
    public BustersHideShow bustersHideAndShow;
    public CircleProgressBar cpb;
    public bool prepareBombBuster = false;
    public bool usingLazerBuster = false;

    public float LazerTime = 6f;
    public float ShieldTime = 6f;
    public float BombTimeBtwExplosions = 0.5f;
    public float TimeBtwHills = 0.5f;

    [Range(0, 100)]
    public int LazerEnergy;
    [Range(0, 100)]
    public int HillEnergy;
    [Range(0, 100)]
    public int BombEnergy;
    [Range(0, 100)]
    public int ShieldEnergy;

    public int EnergyToHill;
    public int EnergyToShield;
    public int EnergyToLazer;
    public int EnergyToBomb;

    public GameObject HillButton;
    public GameObject ShieldButton;
    public GameObject LazerButton;
    public GameObject BombButton;

    public void EnergyGrowth(string cometName, int value)
    {
        if (cometName.Contains("Fire"))
        {
            if(BombEnergy< EnergyToBomb)
                BombEnergy+=value;
            cpb.updateBombCircle();
        }
            
        if (cometName.Contains("Freeze"))
        {
            if (LazerEnergy < EnergyToLazer)
                LazerEnergy += value;
            cpb.updateLazerCircle();
        }

        if (cometName.Contains("Gase"))
        {
            if (HillEnergy < EnergyToHill)
                HillEnergy += value;
            cpb.updateHillCircle();
        }

        if (cometName.Contains("Default"))
        {
            if (ShieldEnergy < EnergyToShield)
                ShieldEnergy += value;
            cpb.updateShieldCircle();
        }

    }

    public bool EnoughEnergyToBuster(string busterTag)
    {
        if (busterTag.Contains("Hill"))
        {
            if (HillEnergy >= EnergyToHill)
            {
                return true;
            }
        }
        else if (busterTag.Contains("Bomb"))
        {
            if (BombEnergy >= EnergyToBomb)
            {
                return true;
            }
        }
        else if (busterTag.Contains("Shield"))
        {
            if (ShieldEnergy >= EnergyToShield)
            {
                return true;
            }
        }
        else if (busterTag.Contains("Lazer"))
        {
            if (LazerEnergy >= EnergyToLazer)
            {
                return true;
            }
        }
        return false;
    }

    public void UsedBombBuster()
    {
        if (prepareBombBuster)
        {
            prepareBombBuster = false;
            BombEnergy -= EnergyToBomb;
            
        }

    }

    public void UsedLazerBuster()
    {
        if (usingLazerBuster)
        {
            usingLazerBuster = false;
        }

    }

    public bool isEnoughLazerEnergy()
    {
        if (LazerEnergy >= EnergyToLazer)
        {
            usingLazerBuster = true;
            LazerEnergy -= EnergyToLazer;
            return true;
        }
        else
        {
            return false;
        }

    }

    public void StartBomb()
    {
        if (BombEnergy >= EnergyToBomb)
        {
            prepareBombBuster = true;
            bustersHideAndShow.HideAllBusters();
        }

    }


}

