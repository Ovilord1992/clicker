﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CometSpawner : MonoBehaviour
{
    public GameObject[] Comets;
    private Transform[] SpawnPoses;
    private Transform[] GiantSpawnPoses;


    [Header("Speed")] 
    [SerializeField]
    private float StartSpeed;
    [SerializeField]
    private float MaxSpeed;
    private float currentSpeed;
    private float deltaSpeed;
    [SerializeField]
    private float IncreasingTime;


    [SerializeField]
    private float RepeatTimeMax;
    [SerializeField]
    private float StartRepeatTime;
    private float currentRepeatTime;
    private float DeltaRepeatTime;




    [Header("Giant Comets")]
    [SerializeField]
    [Range(0f, 1f)]
    private float SpawnChance;
    [SerializeField]
    private float SpawnTryingPeriod;
    [SerializeField]
    private float GiantSpeed;
    [SerializeField]
    private int MinHealth;
    [SerializeField]
    private int MaxHealth;
    [SerializeField]
    [Range(0.05f, 0.5f)]
    private float MinScale;
    [SerializeField]
    [Range(0.05f, 0.5f)]
    private float MaxScale;


    void Start()
    {
        SpawnPoses = GameObject.FindGameObjectWithTag("CometSpawner").GetComponentsInChildren<Transform>();
        GiantSpawnPoses = GameObject.FindGameObjectWithTag("GiantPoses").GetComponentsInChildren<Transform>();
        //изменяем позицию 0 0 0, хз откуда она
        SpawnPoses[0] = SpawnPoses[1];
        GiantSpawnPoses[0] = GiantSpawnPoses[1];
        currentSpeed = StartSpeed;
        currentRepeatTime = StartRepeatTime;

        deltaSpeed = (MaxSpeed - StartSpeed) / IncreasingTime;
        DeltaRepeatTime = (RepeatTimeMax - StartRepeatTime) / IncreasingTime;

        StartCoroutine("StartSpawningCoroutine");
        StartCoroutine("SpeedUp");
        StartCoroutine("TryToSpawnGiant");
    }

    IEnumerator StartSpawningCoroutine()
    {
        while(true)
        {
            yield return new WaitForSeconds(currentRepeatTime);
            SpawnComet();
        }

    }

    void SpawnComet()
    {
        GameObject go = Instantiate(Comets[UnityEngine.Random.Range(0, Comets.Length)],
            SpawnPoses[UnityEngine.Random.Range(0, SpawnPoses.Length)].position,
            Quaternion.identity);
        int value = UnityEngine.Random.Range(3, 7);
        go.GetComponent<Comet>().CometHealth = value;
        go.GetComponent<Comet>().Speed = UnityEngine.Random.Range(currentSpeed- value, currentSpeed);
        go.GetComponent<Transform>().localScale = new Vector3(0.02f* value, 0.02f* value, 0);
    }

    IEnumerator SpeedUp()
    {
        for(int i=0;i< IncreasingTime; i++)
        {
            
            currentSpeed += deltaSpeed;
            currentRepeatTime += DeltaRepeatTime;
            yield return new WaitForSeconds(1f);
        }
        yield return null;
    }

    IEnumerator TryToSpawnGiant()
    {
        while (true)
        {
            yield return new WaitForSeconds(SpawnTryingPeriod);
            if (Random.Range(0f, 1.01f) <= SpawnChance)
            {
                GameObject go = Instantiate(Comets[UnityEngine.Random.Range(0, Comets.Length)],
                    GiantSpawnPoses[UnityEngine.Random.Range(0, GiantSpawnPoses.Length)].position,
                    Quaternion.identity);
                go.GetComponent<Comet>().CometHealth = UnityEngine.Random.Range(MinHealth, MaxHealth);
                go.GetComponent<Comet>().Speed = GiantSpeed;
                float randomScale = Random.Range(MinScale, MaxScale);
                go.GetComponent<Transform>().localScale = new Vector3(randomScale, randomScale, 0);
            }
            
        }

            



    }
}
