﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameStats : MonoBehaviour
{
    public TMP_Text MainScoreText;
    public TMP_Text CominsText;

    [SerializeField]
    private Animator statsAc;
    [SerializeField]
    private Animator cominsAc;

    [HideInInspector]
    public int MainScore;
    [HideInInspector]
    public int MainComins;

    private void Update()
    {
        if (MainScoreText.text != MainScore.ToString())
        {
            statsAc.SetTrigger("scoreUpdated");
            MainScoreText.text = MainScore.ToString();
        }
        if (CominsText.text != MainComins.ToString())
        {
            cominsAc.SetTrigger("cominsUpdated");
            CominsText.text = MainComins.ToString();
        }
    }
}
