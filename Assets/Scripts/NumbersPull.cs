﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumbersPull : MonoBehaviour
{
    private GameObject[] numbersPull;

    private int currIterator = 0;
    void Awake()
    {
        numbersPull = GameObject.FindGameObjectsWithTag("Number");
        foreach (GameObject go in numbersPull)
        {
            go.SetActive(false);
        }
    }

    public void SpawnNumber(Vector3 pos, int number, Color color)
    {
        if (!numbersPull[currIterator].activeInHierarchy)
        {
            numbersPull[currIterator].SetActive(true);
            numbersPull[currIterator].GetComponentInChildren<TMP_Text> ().text = number.ToString();
            numbersPull[currIterator].GetComponentInChildren<TMP_Text>().color = color;
            numbersPull[currIterator].transform.position = pos + new Vector3(Random.Range(-0.3f, 0.3f), Random.Range(-0.3f, 0.3f), 0);
            StartCoroutine(setActiveFalseToNumber(numbersPull[currIterator]));
            currIterator++;
            currIterator %= numbersPull.Length;
        }
    }

    IEnumerator setActiveFalseToNumber(GameObject go)
    {
        yield return new WaitForSeconds(1f);
        go.GetComponentInChildren<TMP_Text>().text = "1";
        go.SetActive(false);
        yield break;
    }
}
