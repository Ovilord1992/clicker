﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;


public class MusicManager : MonoBehaviour
{
    [SerializeField]
    AudioMixerGroup am;

    const string MUSIC = "Music";

    [SerializeField]
    GameObject Stick;

    [SerializeField]
    Slider slider;

    private void Start()
    {
        if (PlayerPrefs.HasKey(MUSIC))
        {
            float temp = PlayerPrefs.GetFloat(MUSIC);
            ChangeMusicVolume(temp);
            slider.value = temp;
        }
        else
        {
            PlayerPrefs.SetInt(MUSIC, 100);
            ChangeMusicVolume(100);
            slider.value = 100;
        }
    }

    public void ChangeMusicVolume(float volume)
    {
        am.audioMixer.SetFloat("MusicVolume", Mathf.Lerp(-80, 0, volume));
        PlayerPrefs.SetFloat(MUSIC, volume);
        if (volume == 0)
        {
            Stick.SetActive(true);
        }
        else
        {
            Stick.SetActive(false);
        }
    }
}






//public void ChangeButton()
//{
//    if (PlayerPrefs.GetInt(MUSIC) > 0)
//    {
//        SetMusicOff();
//
//    }
//    else
//    {
//        SetMusicOnFull();
//    }
//}
//
//public void SetMusicOnFull()
//{
//    PlayerPrefs.SetInt(MUSIC, 5);
//    Stick.SetActive(false);
//    am.audioMixer.SetFloat("MusicVolume", 0);
//    ChangeMusicVolume(0);
//
//}
//
//public void SetMusicOff()
//{
//    PlayerPrefs.SetInt(MUSIC, -5);
//    Stick.SetActive(true);
//    am.audioMixer.SetFloat("MusicVolume", -80);
//    ChangeMusicVolume(-80);
//
//}