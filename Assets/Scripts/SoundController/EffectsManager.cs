﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;


public class EffectsManager : MonoBehaviour
{
    [SerializeField]
    AudioMixerGroup am;

    const string EFFECTS = "Effects";

    [SerializeField]
    GameObject Stick;
<<<<<<< HEAD

    private void Awake()
    {
        if (PlayerPrefs.HasKey(EFFECTS))
        {
            if (PlayerPrefs.GetInt(EFFECTS) > 0)
                SetEffectsOn();
            else
                SetEffectsOff();
        }
        else
        {
            PlayerPrefs.SetInt(EFFECTS, 1);
            SetEffectsOn();
        }
    }

    public void ChangeButton()
    {
        if (PlayerPrefs.GetInt(EFFECTS) > 0)
        {
            SetEffectsOff();
            
        }
        else
        {
            SetEffectsOn();
        }
    }

    public void SetEffectsOn()
    {
        PlayerPrefs.SetInt(EFFECTS, 5);
        Stick.SetActive(false);
        am.audioMixer.SetFloat("EffectsVolume", 0);

    }

    public void SetEffectsOff()
    {
        PlayerPrefs.SetInt(EFFECTS, -5);
        Stick.SetActive(true);
        am.audioMixer.SetFloat("EffectsVolume", -80);
=======
    [SerializeField]
    Slider slider;

    private void Start()
    {
        if (PlayerPrefs.HasKey(EFFECTS))
        {
            float temp = PlayerPrefs.GetFloat(EFFECTS);
            ChangeEffectsVolume(temp);
            if(slider)
                slider.value = temp;
        }
        else
        {
            PlayerPrefs.SetInt(EFFECTS, 100);
            ChangeEffectsVolume(100);
            if (slider)
                slider.value = 100;
        }
    }

    public void ChangeEffectsVolume(float volume)
    {
        am.audioMixer.SetFloat("EffectsVolume", Mathf.Lerp(-80, 0, volume));
        PlayerPrefs.SetFloat(EFFECTS, volume);
        if (volume == 0)
        {
            Stick.SetActive(true);
        }
        else
        {
            Stick.SetActive(false);
        }
    }

    public void InverseEffectsVolume()
    {
        if (PlayerPrefs.GetFloat(EFFECTS)>0)
        {
            ChangeEffectsVolume(0);
            Stick.SetActive(true);
        }
        else
        {
            ChangeEffectsVolume(100);
            Stick.SetActive(false);
        }

>>>>>>> alex

    }

}
<<<<<<< HEAD
=======



//    public void ChangeButton()
//    {
//        if (PlayerPrefs.GetInt(EFFECTS) > 0)
//        {
//            SetEffectsOff();
//            
//        }
//        else
//        {
//            SetEffectsOn();
//        }
//    }
//
//    public void SetEffectsOn()
//    {
//        PlayerPrefs.SetInt(EFFECTS, 5);
//        Stick.SetActive(false);
//        am.audioMixer.SetFloat("EffectsVolume", 0);
//        ChangeEffectsVolume(0);
//
//    }
//
//    public void SetEffectsOff()
//    {
//        PlayerPrefs.SetInt(EFFECTS, -5);
//        Stick.SetActive(true);
//        am.audioMixer.SetFloat("EffectsVolume", -80);
//        ChangeEffectsVolume(-80);
//    }
>>>>>>> alex
