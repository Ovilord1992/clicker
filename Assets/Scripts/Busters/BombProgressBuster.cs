﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombProgressBuster : MonoBehaviour
{
    private float BusterCoast;
    private float CurrentBusterScore;

    public BusterController bc;
    void Start()
    {
        BusterCoast = bc.EnergyToBomb;
        CurrentBusterScore = bc.BombEnergy; 
    }


    private void Update()
    {
        CurrentBusterScore = bc.HillEnergy;
        float rel = CurrentBusterScore / BusterCoast;
        Color c = gameObject.GetComponent<Image>().color;
        c.a = rel;
        gameObject.GetComponent<Image>().color = c;

    }
}
