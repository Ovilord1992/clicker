﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HillProgressBuster : MonoBehaviour
{
    private float BusterCoast;
    private float CurrentBusterScore;

    public BusterController bc;
    void Start()
    {
        BusterCoast = bc.EnergyToHill;
        CurrentBusterScore = bc.HillEnergy;
    }


    public void SetState()
    {
        CurrentBusterScore = bc.HillEnergy;
        float rel = CurrentBusterScore / BusterCoast;
        Color c = gameObject.GetComponent<Image>().color;
        c.a = rel;
        gameObject.GetComponent<Image>().color = c;
        gameObject.GetComponent<Image>().fillAmount = rel;
    }
}
