﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Comet : MonoBehaviour
{
    public float Speed;
    public int CometHealth;

    private GameObject Planet;
    private GameObject bc;
    private Animator ac;

    public GameObject deathEffect;

    [SerializeField]
    GameObject Coin;

    private Rigidbody2D rb;
    private string ThisName;

    Vector2 aim;

    void Start()
    {
        ThisName = transform.name;
        ac = GetComponentInChildren<Animator>();
        Planet = GameObject.FindGameObjectWithTag("Planet");
        rb = GetComponent<Rigidbody2D>();
        aim = Planet.transform.position;
        bc = GameObject.FindWithTag("BusterController");
    }



    void Update()
    {
        GetComponentInChildren<TMP_Text>().text = CometHealth.ToString();
        Vector2 direction = new Vector2(aim.x - transform.position.x, aim.y - transform.position.y);
        rb.AddForce(direction.normalized * Speed * Time.deltaTime);

        if (CometHealth <= 0)
        {
            //bc.GetComponent<BusterController>().EnergyGrowth(transform.name);
            GameObject go = deathEffect;

            ParticleSystem.MainModule psMain = go.GetComponent<ParticleSystem>().main;
            if (ThisName.Contains("Fire"))
            {
                
                psMain.startColor = Color.red;
            }

            if (ThisName.Contains("Freeze"))
            {
                psMain.startColor = Color.blue;
            }

            if (ThisName.Contains("Gase"))
            {
                psMain.startColor = Color.green;
            }

            if (ThisName.Contains("Default"))
            {
                psMain.startColor = Color.grey;
            }
            
            Instantiate(go, gameObject.transform.position, Quaternion.identity);
            SpawnCoin(transform.position);
            Destroy(gameObject);
            
        }
            
    }

    public void SpawnCoin(Vector3 cometPos)
    {
        float a = Random.Range(-0.5f, 0.5f);
        Vector3 newPos = cometPos + new Vector3(a, a, 1);
        Instantiate(Coin, newPos, Quaternion.identity);
    }

    public void GetHitted(int damage)
    {
        CometHealth -= damage;
    }

}
