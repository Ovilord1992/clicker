﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactiveScript : MonoBehaviour
{
    [SerializeField]
    GameObject MainPanel;

    public void Deactive()
    {
        gameObject.SetActive(false);
        MainPanel.SetActive(true);
    }
}
