﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BustersAnims : MonoBehaviour
{
    [SerializeField]
    Animator ac;
    BusterController bc;

    private void Awake()
    {
        bc = FindObjectOfType<BusterController>();
    }
    public void NotEnoughEnergy()
    {
        if (!bc.EnoughEnergyToBuster(transform.tag))
                ac.SetTrigger("NotEnoughEnergy");
            
    }

    private void FixedUpdate()
    {
        if (bc.EnoughEnergyToBuster(transform.tag))
            ac.SetBool("IsReady", true);
        else
        {
            ac.SetBool("IsReady", false);
        }
        

    }








}
