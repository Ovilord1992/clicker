﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BombBuster : MonoBehaviour
{
    public float bombRadius;
    public int bombDamage;

    public CircleProgressBar cpb;
    [SerializeField]
    public GameStats GameStats;
    [SerializeField]
    private BusterController bc;

    [SerializeField]
    private GameObject explosionEffect;


    [SerializeField]
    NumbersPull np;

    public Camera Cam;
    public Animator CamAnim;

    private void Update()
    {
        if (bc.GetComponent<BusterController>().prepareBombBuster)
        {
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    SpawnBomb(Cam.ScreenToWorldPoint(Input.GetTouch(0).position));
                    bc.UsedBombBuster();
                    bc.GetComponent<BustersHideShow>().BombInvokeShowAllBusters();
                    cpb.updateBombCircle();

                }
            }
        }
    }
    public void SpawnBomb(Vector2 bombPos)
    {
        CamAnim.SetTrigger("Shake");
        Instantiate(explosionEffect, bombPos, Quaternion.identity);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(bombPos, bombRadius);
        foreach(Collider2D col in colliders)
        {
            Transform obj = col.transform;
            if (obj.CompareTag("Comet") )
            {
                int health = obj.GetComponent<Comet>().CometHealth; ;
                GameStats.MainScore += health;
                bc.EnergyGrowth(obj.transform.name, health);
                np.SpawnNumber(obj.transform.position, health,Color.red);
                obj.GetComponent<Comet>().CometHealth -= bombDamage;
                GameStats.MainScore += bombDamage;
                

            }
            if (obj.CompareTag("Planet"))
            {
                
                obj.GetComponent<Planet>().PlanetHealth -= bombDamage;
                obj.GetComponent<Planet>().CheckHealth();


            }
            
        }
        
    }






}
