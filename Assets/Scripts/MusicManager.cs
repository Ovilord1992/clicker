﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;


public class MusicManager : MonoBehaviour
{
    [SerializeField]
    AudioMixerGroup am;

    const string MUSIC = "Music";

    [SerializeField]
    GameObject Stick;

    private void Awake()
    {
        if (PlayerPrefs.HasKey(MUSIC))
        {
            if (PlayerPrefs.GetInt(MUSIC) > 0)
                SetMusicOn();
            else
                SetMusicOff();
        }
        else
        {
            PlayerPrefs.SetInt(MUSIC, 1);
            SetMusicOn();
        }
    }

    public void ChangeButton()
    {
        if (PlayerPrefs.GetInt(MUSIC) > 0)
        {
            SetMusicOff();

        }
        else
        {
            SetMusicOn();
        }
    }

    public void SetMusicOn()
    {
        PlayerPrefs.SetInt(MUSIC, 5);
        Stick.SetActive(false);
        am.audioMixer.SetFloat("MusicVolume", 0);
        

    }

    public void SetMusicOff()
    {
        PlayerPrefs.SetInt(MUSIC, -5);
        Stick.SetActive(true);
        am.audioMixer.SetFloat("MusicVolume", -80);

    }

}
